Docker image for Symfony
========================

Hub: https://hub.docker.com/r/spam312sn/symfony

## Usage:

```
docker pull registry.gitlab.com/spam312sn/docker-symfony:<tag>
```

## Tags:

- `php7.2-xdebug`
- `php7.4-xdebug`

## Software:

- PHP (https://github.com/docker-library/php)
    - v7.2
    - v7.4
- Composer v2
- Xdebug

## Extensions:

- intl
- pcntl
- pdo_mysql
- mysqli
- opcache
- apcu
- zip
