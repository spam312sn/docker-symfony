FROM php:7.2-fpm

MAINTAINER Serghei Niculaev <s.niculaev@protonmail.ch>

RUN apt-get clean
RUN apt-get update > /dev/null

RUN apt-get install -y --no-install-recommends apt-utils > /dev/null

# Install intl extension
RUN apt-get install -y zlib1g-dev libicu-dev g++ > /dev/null
RUN docker-php-ext-configure intl > /dev/null
RUN docker-php-ext-install intl > /dev/null

# Install pcntl extension
RUN docker-php-ext-install pcntl > /dev/null

# Install MySQL PDO
RUN docker-php-ext-install pdo_mysql > /dev/null
RUN docker-php-ext-install mysqli > /dev/null

# Install OPcache
RUN docker-php-ext-install opcache > /dev/null

# Install APCu
RUN pecl install apcu-5.1.17 > /dev/null
RUN docker-php-ext-enable apcu > /dev/null

# Install ZIP extension
RUN apt-get install -y libzip-dev zip unzip > /dev/null
RUN docker-php-ext-configure zip --with-libzip > /dev/null
RUN docker-php-ext-install zip > /dev/null

# Install composer
RUN apt-get install -y git > /dev/null
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

# Install Xdebug
RUN pecl install xdebug > /dev/null
RUN docker-php-ext-enable xdebug > /dev/null
RUN docker-php-source delete

#COPY docker/php/php.ini /usr/local/etc/php/conf.d/
COPY ./php.ini /usr/local/etc/php/
COPY ./php-fpm.conf /etc/php-fpm.conf

RUN PATH=$PATH:/usr/src/apps/vendor/bin:bin

EXPOSE 9000
CMD ["php-fpm", "-F"]
